<h1 align="center">Aster-X Registry</h1>

[[_TOC_]]

## Description

This repository is a fork of GXDCH Registry. It is intended to be used both by GXFSFR fork of GXDCH Compliance as well as the main registry of the Ecosystem limited component such as the [Aster-X conformity checker](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-conformity).

The main point of divergence between this registry and the GXDCH revolves arround the flexibility of the terms of condition with two additionnal method to add and modify Terms And condition. Those methods are protected by an API-KEY that can be set in the Environment variables (API_KEY). Ecosystem Terms and Conditions are supposed to extend Gaia-X own terms. Therefore implementors SHOULD state the need for GXDCH Compliance in their own terms and conditions.

Another point of divergence concerns the lifecycle mecanism which can be implemented in the ecosystem. As per the latest draft of lifecycle mecanism, the most recent method is to use Bitstring Status List. Bitstring Status List extends the previous Status List 2021 by adding a new kind of list: the message. 

The message list can associate state to a specific messages, therefore it allows issuer to generate a list that can manage both suspension and revocation as well as any message an issuer may need to use to maintain the lifecycle of a credential. To enable interoperrability within the ecosystem, implementors should maintain a list of all possible messages used by participants in the ecosystem. Therefore this registry is able to host and expose this kind of messages

This registry also allows the inclusion of a new trusted issuer which is its own conformity checker. To do so, implementors should refer the did of their conformity rule checker in the Envirnoment variable (CONFORMITY_DID)
## Get Started with Using The API

You can find the Swagger API documentation under `localhost:3000/docs/` or https://registry.lab.gaia-x.eu/docs/


## Setup ecosystem Lifecycle Messages 

To setup the messages, you can you use the route `/api/lifecycle` as a Post Request to update a new set of messages. This route is secured by an API_KEY that can be setup in the environment variables.

The body of the request is such as : 
```
[
  {
    "status": "0x00",
    "value": "VALID"
  },
  {
    "status": "0x01",
    "value": "SUSPENDED"
  },
  {
    "status": "0x10",
    "value": "REVOKED"
  },
  {
    "status": "0x11",
    "value": "EXPIRED"
  }
]
```

Note that the length of the messages is related to the size of each entry in the status list. a size of 1 would display 2^1 message (2), a size 2 would display 2² (4) and so on. This example describes a list of size two. 

## Get the ecosystem Lifecycle Messages

To get all messages, you can use the same route with a GET method.


## Setup ecosystem Terms and conditions 

Using the `/api/termsAndConditions` , you can setup or get the ecosystem terms and condition depending on the REST method used.

To setup the terms and conditions, you can use a POST Request with as body:

```json
{
  "text": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n- to be and remain GaiaX compliant while being a member of this Federation\nThe Federation will revoke any Verifiable Credential emmitted by the Federation if the Federation becomes aware of any inaccurate statement in regards to the two rules listed \n"
}
```

You can also add a parameter version if you want to manage versionning in your ecosystem

**Responses:**

If the terms and conditions are added successfully, a HTTP response status code `200` will be sent back

### Get Terms and conditions 

To get Terms and conditions, use the same route with a GET method. You can specify the parameter version if you manage different version in your ecosystem.

**Responses:**

If the terms and conditions have been set, a HTTP response status code `200` will be sent back and the terms and conditions as a string


### Look for a given Trust Anchor public key in the registry

Using the `/api/trustAnchor` route, you can check if a given public key exists in the list of endorsed trust anchors.
You must provide the public key in the request body.

**Request body:**

```json
{
  "certificate": "MIIE0TCCA7mgAwIBAgICApMwDQYJKoZIhvcNAQEFBQAwgc8xCzAJBgNVBAYTAkFUMYGLMIGIBgNVBAoegYAAQQAtAFQAcgB1AHMAdAAgAEcAZQBzAC4AIABmAPwAcgAgAFMAaQBjAGgAZQByAGgAZQBpAHQAcwBzAHkAcwB0AGUAbQBlACAAaQBtACAAZQBsAGUAawB0AHIALgAgAEQAYQB0AGUAbgB2AGUAcgBrAGUAaAByACAARwBtAGIASDEYMBYGA1UECxMPQS1UcnVzdC1RdWFsLTAxMRgwFgYDVQQDEw9BLVRydXN0LVF1YWwtMDEwHhcNMDIwMjA2MjMwMDAwWhcNMDUwMjA2MjMwMDAwWjCB0TELMAkGA1UEBhMCQVQxgYswgYgGA1UECh6BgABBAC0AVAByAHUAcwB0ACAARwBlAHMALgAgAGYA/AByACAAUwBpAGMAaABlAHIAaABlAGkAdABzAHMAeQBzAHQAZQBtAGUAIABpAG0AIABlAGwAZQBrAHQAcgAuACAARABhAHQAZQBuAHYAZQByAGsAZQBoAHIAIABHAG0AYgBIMRkwFwYDVQQLExBUcnVzdFNpZ24tU2lnLTAxMRkwFwYDVQQDExBUcnVzdFNpZ24tU2lnLTAxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3vqE78j1NAlAT8GC1kqNk6qL+jn1dLTADCiQVIyct01vkO0zINkUg2x6k7E2xnmyDPg4ihayczDYcuzzQMMms63MFDlONMZ180CxFsZpOYK8SctdTWdH0tGJFJF0oBWI6ROykMxo3knPLMDMTS586MgPdKxmihk5OjyZvqiEH1SdmuFR4UTke81cisUkM6+hh4v/AYrPShXOlEmNmFILMw9iQST5aeylmgfWwixCZgnmS7fr3j65gezxhPN56qRyndgu97/9VsKMASYTsaD8pZEZ230TIopuwGb4AeO7CUrU22scpnreHLW1ZX4hldJYJXzFpBH2pAnHfiQgADs4CwIDAQABo4GyMIGvMA8GA1UdEwEB/wQFMAMBAf8wEQYDVR0OBAoECEeMFJHO+gwZMBMGA1UdIwQMMAqACEs8jB2F6W+tMA4GA1UdDwEB/wQEAwIBBjBkBgNVHR8EXTBbMFmgV6BVhlNsZGFwOi8vbGRhcC5hLXRydXN0LmF0L291PUEtVHJ1c3QtUXVhbC0wMSxvPUEtVHJ1c3QsYz1BVD9jZXJ0aWZpY2F0ZXJldm9jYXRpb25saXN0PzANBgkqhkiG9w0BAQUFAAOCAQEAS1Ch3C9IGqX4c/PCp0Hp93r+07ftu+A77EW7w5kDQ6QhuuXAJgVk4Vyp4D+wlJMxQpvDGkBnaSYcRyT5rT7ie/eImbHi+ep6AD/mcKs8D2XXDDEy/UxuvEe7pDqrAZc93LID8HnrMf2OFqXbmBggN/TvD20s2CBnEgP+QCm9asttMCg1FLIAIeDL/JstH3ddTlIQNPgUCCaUmATP8oOnh2eon7Fn5+dnKDUY4j6lRDPsDzu0wU0s9VWiyS9Lay0f7P9h3LcYHtAYZg2BPlEAqpOVNVXZ+4JdjRKvhHh9pgadnGNkunrenDiekHgbABop0s2yj5EDAkqYIosnp4bOCQ=="
}
```

**Responses:**

If a corresponding trust anchor is found, a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
{
  "trustState": "trusted",
  "trustedForAttributes": "/.*/gm",
  "trustedAt": 1645817070
}
```

**Negative responses:**
|HTTP response status code |Description |
|--- | --- |
|`404`| Could not find a Trust Anchor for the given public key|
|`400`| Invalid request|

### Verify that a given Certificate Chain is resolvable against a Trust Anchor in the registry

Using the `/api/trustAnchor/chain` route, you can check if any of the given certificates in a certificate chain is resolvable against an endorsed trust anchor.
You must provide the certificate chain as `String` in the request body. Make sure to provide a cleared `String` without spaces and new line characters.

**Request body (certificates shortened in example):**

```json
{
  "certs": "-----BEGIN CERTIFICATE-----MIIDxzCCA02gAwIBAgISAwrdU+...sF6DpICK1MndBOGAY5E5RDu1EW6+Snk852PQTDM=-----END CERTIFICATE----------BEGIN CERTIFICATE-----MIICxjCCAk2gAwIBAgIRALO93.../Lgul/-----END CERTIFICATE----------BEGIN CERTIFICATE-----MIICGzCCAaGgAwIBAgIQQdKd0XLq7qeAwSxs6S...q4AaOeMSQ+2b1tbFfLn-----END CERTIFICATE-----"
}
```

**Responses:**

If a corresponding trust anchor is found, a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
{
  "result": true
}
```

**Negative responses:**

HTTP response status code `409` - Certificate chain could not be verified:

```json
{
  "result": false,
  "resultCode": -1,
  "resultMessage": "No valid certificate paths found"
}
```

| HTTP response status code | Description     |
| ------------------------- | --------------- |
| `400`                     | Invalid request |

## Get Started With Development

---
**NOTE**

For details on how the code is structured and how to create a Merge Request
please check the instructions from CONTRIBUTING.md

---

Make sure docker and docker-compose are available on your setup.
Clone the repository and jump into the newly created directory:

```sh
git clone https://gitlab.com/gaia-x/lab/compliance/gx-registry.git
cd gx-registry
```

Next we need to take care of the initial setup of the project:

```sh
# Install all the dependencies
npm install

# Make sure the ./dist folder exists
mkdir ./dist

# Create a .env file or use the example:
# The PORT .env variable is required to be set
# This file will be used by the docker-compose command to set the environment variables in the gx-registry-server container
mv .env.example .env

# Make sure npx is installed, as it is used for our commitlint setup
npm install -g npx
```

MongoDB v5.0 and newer versions have a known dependency on AVX flag being enabled on the CPU.
If the dev environment does not have this capability, pin the version of the
mongo image to the latest v.4.x in the dockerfile
```sh
....
services:
  mongo:
    image: mongo:4.4.18
    container_name: gx-registry-mongo
....
```

If everything is setup correctly, you can start the development environment with docker-compose. Make sure that the Docker daemon is running on your host operating system.

```sh
docker-compose up
```

### Default Setup

Credits to the (typescript-express-starter)[https://github.com/ljlm0402/typescript-express-starter#readme] repository at https://github.com/ljlm0402/typescript-express-starter#readme. This repository uses a customized & enhanced version of the `Mongoose` template.

- Typesript enabled
- Prettier setup with husky to follow & enforce code styling standards upon commits
- Swagger documentation via a `./swagger.yml` file, available at `[host]/docs`
- Dockerfile to be used in `development` & `production` environments
- Quick development setup via `docker compose` -> `docker compose up` will serve `localhost:3000`
- VSCode Extensions and on-save formatting
- Sample K8 deployment files for easy MongoDB & Server pod deployments, located at `deploy/[mongo/server]-deployment.yaml`
