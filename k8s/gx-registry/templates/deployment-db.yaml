apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "gx-registry.fullname" . }}-mongo
  labels:
    {{- include "gx-registry.labelsMongo" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "gx-registry.selectorLabelsMongo" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "gx-registry.selectorLabelsMongo" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        ## MONGO
        - name: {{ include "gx-registry.fullname" . }}-mongo
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "mongo"
          args: ["--dbpath","/data/db"]
          imagePullPolicy: IfNotPresent
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          env:
            - name: MONGO_INITDB_ROOT_USERNAME
              valueFrom:
                secretKeyRef:
                  name: {{ include "gx-registry.fullname" . }}-mongo-creds
                  key: username
            - name: MONGO_INITDB_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ include "gx-registry.fullname" . }}-mongo-creds
                  key: password
          volumeMounts:
            - name: {{ include "gx-registry.fullname" . }}-data-dir
              mountPath: "/data/db"
            - name: {{ include "gx-registry.fullname" . }}-mongo-init
              mountPath: "/docker-entrypoint-initdb.d"
          ports:
            - name: mongo
              containerPort: 27017
              protocol: TCP
          livenessProbe:
            exec:
              command:
                - mongosh
                - --eval
                - "db.adminCommand('ping')"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 10
            successThreshold: 1
            failureThreshold: 6
          readinessProbe:
            exec:
              command:
                - mongosh
                - --eval
                - "db.adminCommand('ping')"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 10
            successThreshold: 1
            failureThreshold: 6
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      ## END MONGO
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: {{ include "gx-registry.fullname" . }}-data-dir
          persistentVolumeClaim:
            claimName: {{ include "gx-registry.fullname" . }}-mongo-data-pvc
        - name: {{ include "gx-registry.fullname" . }}-mongo-init # Db init script
          configMap:
            name: {{ include "gx-registry.fullname" . }}-mongo-init
            items:
              - key: init.js
                path: init.js