db.auth('mongoroot', 'password123')

db = db.getSiblingDB('trust-anchor-registry')


db.createUser(
  {
    user: "mongoadmin",
    pwd: "password123",
    roles: [
      { role: "userAdmin", db: "trust-anchor-registry" },
      { role: "readWrite", db: "trust-anchor-registry" },
      { role: "dbAdmin", db: "trust-anchor-registry" }
    ]
  }
)
