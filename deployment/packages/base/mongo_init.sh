mongosh -- "$MONGO_INITDB_DATABASE" <<EOF
    var rootUser = '$DB_USERNAME';
    var rootPassword = '$DB_PASSWORD';
    var initDB = '$MONGO_DATABASE';

    var admin = db.getSiblingDB(initDB);
    admin.auth(rootUser, rootPassword);

    db = db.getSiblingDB('trust-anchor-registry');
    db.createUser(
  {
    user: rootUser,
    pwd: rootPassword,
    roles: [
      { role: "userAdmin", db: "trust-anchor-registry" },
      { role: "readWrite", db: "trust-anchor-registry" },
      { role: "dbAdmin", db: "trust-anchor-registry" }
    ]
  });

EOF