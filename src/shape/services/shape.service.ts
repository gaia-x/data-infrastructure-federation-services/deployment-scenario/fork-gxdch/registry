import { Injectable, NotFoundException } from '@nestjs/common'
import { join } from 'path'
import fs from 'fs'
import * as ttl2jsonld from '@frogcat/ttl2jsonld'
import { FileTypes } from '../constants'

@Injectable()
export class ShapeService2210 {
  getFileByType(file: string, type: FileTypes, version?: string): string {
    try {
      if (FileTypes.jsonld === type) {
        return this.getJsonLdShape(file, version)
      }
      return this.getTTLFile(file, version)
    } catch (e) {
      throw new NotFoundException('Desired shaped not found, shape incorrect or unimplemented')
    }
  }

  getJsonLdShape(key: string, version?: string): any {
    const ttlFile = this.getTTLFile(key, version)
    return this.convertToJsonld(ttlFile)
  }

  async getJsonldContext(): Promise<any> {
    try {
      const context = {
        '@version': 2210
      }

      const shapeFiles = await this.getAvailableShapeFiles('latest')
      for (const file of shapeFiles) {
        const ttlFile = this.getTTLFile(file, 'latest')
        context[`gx-${file}`] = this.convertToJsonld(ttlFile)
      }

      return context
    } catch (e) {
      console.log('error', e.message)
      throw e
    }
  }

  async getAvailableShapeFiles(version?: string): Promise<Array<string>> {
    try {
      return fs.readdirSync(join(__dirname, `../../static/shapes/${version}`)).map(filename => filename.split('.')[0])
    } catch (e) {
      throw new NotFoundException('Desired shaped not found, version is incorrect or unimplemented')
    }
  }

  // async uploadShape(version: string, shape, name: string): Promise<void> {
  //   await fs.writeFile(`../../static/shapes/${version}/${name}.ttl`, shape, (() => { return true }));
  // }

  private getTTLFile(filename, version?: string): string {
    try {
      let aliasedFileName
      if (!version) {
        version = 'latest'
      }

      if (version === 'development') {
        aliasedFileName = filename
      } else {
        aliasedFileName = this.aliasParticipantSOTAC(filename)
      }

      fs.accessSync(join(__dirname, `../../static/shapes/${version}/${aliasedFileName}.ttl`))
      const ttl = fs.readFileSync(join(__dirname, `../../static/shapes/${version}/${aliasedFileName}.ttl`))
      return ttl.toString().replace(/https:\/\/\$BASE_URL\$/g, process.env.BASE_URL)
    } catch (Error) {
      throw new NotFoundException('Desired shape not found, shape name is incorrect or unimplemented')
    }
  }

  private convertToJsonld(ttl: string): any {
    return ttl2jsonld.parse(ttl)
  }

  private aliasParticipantSOTAC(filename) {
    if ('participant' === filename || 'service-offering' === filename || 'termsandconditions' === filename) {
      return 'trustframework'
    }
    return filename
  }
}
