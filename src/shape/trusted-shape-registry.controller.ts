import { Body, Controller, Get, Param, Post, Query, Response } from '@nestjs/common'
import { ApiBody, ApiOkResponse, ApiOperation, ApiQuery, ApiTags, ApiHeader } from '@nestjs/swagger'
import { ShapeService2210 } from './services'
import { ShapeFilesApiResponse } from './decorators'
import { FileTypes } from './constants'
import { Response as Res } from 'express'

@ApiTags('Trusted-Shape-registry')
@Controller({ path: '/api/trusted-shape-registry/v1/shapes' })
export class TrustedShapeRegistry {
  constructor(private readonly shapesService: ShapeService2210) {}

  @ApiOperation({ summary: 'Get a JSONLD context for all available shapes (2210).' })
  @ApiOkResponse({ description: 'The JSONLD context for all available shapes in the registry.' })
  @Get()
  async getVersion(): Promise<any> {
    return this.shapesService.getJsonldContext()
  }

  @ApiOperation({ summary: 'Get the list of implemented shapes' })
  @ApiOkResponse({ description: 'List of implemented TrustFramework shapes.' })
  @Get('implemented')
  getImplementedShapes(): string[] {
    return ['LegalParticipant', 'ServiceOffering', 'legalRegistrationNumber']
  }

  @ApiOperation({ summary: 'Get a JSONLD shape' })
  @ApiOkResponse({ description: 'The JSONLD shape.' })
  @Get('/jsonld/:key')
  @ApiQuery({
    name: 'v',
    type: 'string',
    description: 'Optional shape version ID. Default to latest',
    required: false
  })
  async getJSONLDShape(@Param('key') key: string, @Response({ passthrough: true }) res: Res, @Query('v') v?: string): Promise<any> {
    res.set({ 'Content-Type': `application/ld+json` })
    return this.shapesService.getJsonLdShape(key, v)
  }

  @ShapeFilesApiResponse('Get specified SHACL file as ttl according to its identifier')
  @Get('/:key')
  getShapeVersioned(@Param('key') key, @Response({ passthrough: true }) res: Res, @Query('v') v?: string): string {
    try {
      const shape = this.shapesService.getFileByType(key, FileTypes.ttl, v)
      res.set({ 'Content-Type': `text/turtle` })
      return shape
    } catch (e) {
      throw e
    }
  }

  // @ApiOperation({ summary: 'Upload a shape' })
  // @ApiOkResponse({ description: 'Ok.' })
  // @ApiBody({ type: String, description: 'Turtle data' })
  // @Post()
  // async postShape(@Query('version') version: string, @Query('name') name: string, @Body() ttl: string): Promise<void> {
  //   try {
  //     console.log(ttl, version, name)
  //     await this.shapesService.uploadShape(version, ttl, name)
  //   } catch (e) {
  //     throw e
  //   }
  // }
}
