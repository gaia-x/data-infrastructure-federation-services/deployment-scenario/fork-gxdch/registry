import { Test, TestingModule } from '@nestjs/testing'
import { TrustedShapeRegistry } from './trusted-shape-registry.controller'
import { ShapeService2210 } from './services'
import { FileTypes } from './constants'

describe('AppController', () => {
  let appController: TrustedShapeRegistry
  let Shape: ShapeService2210
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [TrustedShapeRegistry, ShapeService2210],
      providers: [ShapeService2210]
    }).compile()

    appController = app.get<TrustedShapeRegistry>(TrustedShapeRegistry)
    Shape = app.get<ShapeService2210>(ShapeService2210)
  })

  describe('Get all 2210 shapes', () => {
    it('should return all the 2210 shapes in an object', async () => {
      const shapes = await appController.getVersion()
      expect(typeof shapes).toEqual('object')
      expect(Object.keys(shapes).length).toBeGreaterThan(2)
      expect(Object.keys(shapes).includes('@context'))
    })
  })
  describe('Get a specific shape', () => {
    it('should return a specific shape readstream given a unique identifier', async () => {
      const shape = Shape.getFileByType('participant', FileTypes.ttl)
      expect(shape.indexOf('gx:LegalParticipant')).toBeGreaterThan(-1)
    }, 10000)
  })
  describe('Return error when wanted shape is not available', () => {
    it('should throw a 404 error when using a non uploaded identifier', async () => {
      try {
        await Shape.getFileByType('particidddpant', FileTypes.ttl)
        fail()
      } catch (e) {
        expect(e.status).toEqual(404)
      }
    })
  })
})
