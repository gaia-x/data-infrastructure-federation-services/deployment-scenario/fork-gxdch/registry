import { TrustedIssuersService } from './trusted-issuers.service'
import { HttpService } from '@nestjs/axios'
import { of } from 'rxjs'

const servicesYaml = `
registry:
  - registry.domain.tld
`

const axiosResponse = {
  data: 'Components',
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {}
}
describe.skip('TrustedIssuersService', () => {
  const httpService = new HttpService()
  const trustedIssuersService = new TrustedIssuersService(httpService)

  beforeAll(() => {
    axiosResponse.data = servicesYaml
    jest.spyOn(httpService, 'get').mockImplementation(() => of(axiosResponse))
  })
  describe('listTrustedIssuers()', () => {
    it('should return the map of trusted anchors by service', () => {
      trustedIssuersService.getTrustedIssuers().then(
        issuers => {
          expect(issuers).toBeDefined()
          expect(issuers.hasOwnProperty('registry')).toBeTruthy()
          expect(issuers['registry']).toBeDefined()
          expect(issuers['registry']).toContain('registry.domain.tld')
        },
        error => {
          fail(`An error occurred ${error}`)
        }
      )
    })
  })
  describe('listTrustedIssuersForAType()', () => {
    it('should return the array of trusted anchors for a service', () => {
      trustedIssuersService.getTrustedIssuersForAServiceType('registry').then(
        issuers => {
          expect(issuers).toBeDefined()
          expect(issuers).toContain('registry.domain.tld')
        },
        error => {
          fail(`An error occurred ${error}`)
        }
      )
    })
    it('should return an empty of trusted anchors for a service that is not listed', () => {
      trustedIssuersService.getTrustedIssuersForAServiceType('test').then(
        issuers => {
          expect(issuers).toBeDefined()
          expect(issuers.length).toBe(0)
        },
        error => {
          fail(`An error occurred ${error}`)
        }
      )
    })
  })
})
