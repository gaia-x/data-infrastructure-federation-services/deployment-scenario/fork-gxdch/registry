import { HttpService } from '@nestjs/axios'
import { Injectable } from '@nestjs/common'
import { firstValueFrom } from 'rxjs'
import YAML from 'yaml'

export type TrustedIssuers = { [key: string]: string[] }

@Injectable()
export class TrustedIssuersService {
  constructor(private httpService: HttpService) { }

  async getTrustedIssuers(): Promise<TrustedIssuers> {
    const request = this.httpService.get('https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/trusted-gxdch.yaml')
    const response = YAML.parse((await firstValueFrom(request)).data)
    response.registry.push(process.env.BASE_URL)
    response.compliance.push(process.env.CONFORMITY_DID)
    response.compliance.push('compliance.lab.gaia-x.eu:development')
    response.compliance.push('compliance.lab.gaia-x.eu:v1-staging')
    return response
  }

  async getTrustedIssuersForAServiceType(service: string): Promise<string[]> {
    return (await this.getTrustedIssuers())[service] ? (await this.getTrustedIssuers())[service] : []
  }
}
