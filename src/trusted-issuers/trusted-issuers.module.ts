import { Module } from '@nestjs/common'
import { TrustedIssuersController } from './trusted-issuers.controller'
import { HttpModule } from '@nestjs/axios'
import { TrustedIssuersService } from './trusted-issuers.service'

@Module({ imports: [HttpModule], controllers: [TrustedIssuersController], providers: [TrustedIssuersService] })
export class TrustedIssuersModule {}
