import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { setupSwagger } from './common/util'

export const appPath = !!process.env['APP_PATH'] ? process.env['APP_PATH'] : ''

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.setGlobalPrefix(`${appPath}/`)
  setupSwagger(app)

  app.enableCors()
  await app.listen(process.env.PORT || 3000)
}

bootstrap()
