import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type TermsAndCoditionDocument = TermsAndCondition & Document

@Schema({
  timestamps: true
})
export class TermsAndCondition {
  @Prop({
    required: true,
    trim: true
  })
  public version: string

  @Prop({
    required: true,
    trim: true
  })
  public terms: string
}

export const TermsAndConditionSchema = SchemaFactory.createForClass(TermsAndCondition)
