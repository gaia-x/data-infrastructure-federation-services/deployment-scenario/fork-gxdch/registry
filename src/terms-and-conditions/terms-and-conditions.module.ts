import { Module } from '@nestjs/common'
import { TermsAndConditionsService } from './services'
import { TermsAndConditionsController } from './terms-and-conditions.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { TermsAndCondition, TermsAndConditionSchema } from './schemas/tc.schema'
import { ApiKeyGuard } from '../auth/auth.guard'

@Module({
  imports: [MongooseModule.forFeature([{ name: TermsAndCondition.name, schema: TermsAndConditionSchema }])],
  controllers: [TermsAndConditionsController],
  providers: [TermsAndConditionsService, ApiKeyGuard]
})
export class TermsAndConditionsModule {}
