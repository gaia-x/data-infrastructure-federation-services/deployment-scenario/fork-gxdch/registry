import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'
import { termsAndConditions } from '../constants'
import { TermsAndCoditionDocument, TermsAndCondition } from '../schemas/tc.schema'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

@Injectable()
export class TermsAndConditionsService {
  constructor(@InjectModel(TermsAndCondition.name) public termsModel: Model<TermsAndCoditionDocument>) {}
  async getAvailableTermsAndConditions(version?: string): Promise<string> {
    if (!version) {
      const tc = await this.termsModel.findOne({ version: 'latest' }).exec()
      if (tc === null) {
        throw new NotFoundException('TC Not founded')
      }
      return tc.terms
    } else {
      console.log(version)
      const tc = await this.termsModel.findOne({ version: version }).exec()
      if (tc === null) {
        throw new NotFoundException('TC Not founded')
      }
      return tc.terms
    }
  }

  async postTermsAndCondition(tc: string, version?: string): Promise<TermsAndCondition> {
    if (!version) {
      const query = await this.termsModel
        .findOne({ version: 'latest' })
        .exec()
        .catch(e => console.error(e))
      if (query == null) {
        const terms = new this.termsModel({ version: 'latest', terms: tc })
        return terms.save()
      } else {
        throw new ForbiddenException('To update this terms and condition, use the apropriate method')
      }
    } else {
      const query = await this.termsModel
        .findOne({ version: version })
        .exec()
        .catch(e => console.error(e))
      if (query == null) {
        const terms = new this.termsModel({ version: version, terms: tc })
        return terms.save()
      } else {
        throw new ForbiddenException('To update this terms and condition, use the apropriate method')
      }
    }
  }

  async updateTermsAndCondition(tc: string, version: string): Promise<void> {
    const query = await this.termsModel
      .findOneAndUpdate({ version: version }, { version: version, terms: tc })
      .exec()
      .catch(e => console.error(e))
    if (query === null) {
      throw new NotFoundException('Terms And condition not found')
    }
  }
}
