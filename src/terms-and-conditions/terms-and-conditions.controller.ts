import { Controller, Get, Post, UsePipes, Query, Put, UseGuards, Body } from '@nestjs/common'
import { ApiBasicAuth, ApiTags, ApiSecurity, ApiBody } from '@nestjs/swagger'
import { TermsAndConditionsService } from './services'
import { TermsAndConditionsApiResponse } from './decorators'
import { ApiQuery } from '@nestjs/swagger'
import { ApiKeyGuard } from '../auth/auth.guard'
import { termsAndConditions } from './constants'
import { TermsAndConditionsDto } from './dto'

@ApiTags('TermsAndConditions')
@Controller({ path: '/api/termsAndConditions' })
export class TermsAndConditionsController {
  constructor(private readonly termsAndConditionsService: TermsAndConditionsService) {}

  @TermsAndConditionsApiResponse('Get terms and conditions for specified version or a list of available versions when no version is specified.')
  @ApiQuery({
    name: 'version',
    type: 'string',
    description: 'Optional shape version ID. Default to latest',
    required: false
  })
  @Get()
  async getTermsAndConditions(@Query('version') version?: string) {
    return this.termsAndConditionsService.getAvailableTermsAndConditions(version)
  }

  @TermsAndConditionsApiResponse('Update terms and conditions for specified version orto the latest one')
  @ApiQuery({
    name: 'version',
    type: 'string',
    description: 'Optional shape version ID. Default to latest',
    required: false
  })
  @ApiBody({
    type: 'string',
    examples: {
      example: {
        value: termsAndConditions
      }
    },
    required: true
  })
  @ApiSecurity('X-API-KEY')
  @UseGuards(ApiKeyGuard)
  @Post()
  async postTermsAndConditions(@Body() terms: TermsAndConditionsDto, @Query('version') version?: string) {
    console.log(terms)
    return this.termsAndConditionsService.postTermsAndCondition(terms.text, version)
  }

  @TermsAndConditionsApiResponse('Update terms and conditions for specified version orto the latest one')
  @ApiQuery({
    name: 'version',
    type: 'string',
    description: 'Optional shape version ID. Default to latest',
    required: true
  })
  @ApiBody({
    type: 'string',
    examples: {
      example: {
        value: termsAndConditions.text
      }
    },
    required: true
  })
  @ApiSecurity('X-API-KEY')
  @UseGuards(ApiKeyGuard)
  @Put()
  async updateTermsAndConditions(@Body() terms: string, @Query('version') version: string) {
    return this.termsAndConditionsService.updateTermsAndCondition(terms, version)
  }
}
