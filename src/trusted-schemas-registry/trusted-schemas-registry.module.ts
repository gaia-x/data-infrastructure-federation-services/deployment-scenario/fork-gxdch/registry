import { Module } from '@nestjs/common'
import { SchemasService2210 } from './service'
import { TrustedSchemasRegistry } from './trusted-schemas-registry.controller'

@Module({
  imports: [],
  controllers: [TrustedSchemasRegistry],
  providers: [SchemasService2210]
})
export class TrustedSchemasRegistry2210Module {}
