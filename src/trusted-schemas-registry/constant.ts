import fs from 'fs'
import { join } from 'path'

export const availableSchemas = fs.readdirSync(join(__dirname, '../static/schemas')).map(filename => filename.split('.')[0])

export enum FileTypes {
  ttl,
  jsonld
}
