import { Injectable, Logger, NotFoundException } from '@nestjs/common'
import { join } from 'path'
import fs from 'fs'
import { FileTypes } from '../../shape/constants'

@Injectable()
export class SchemasService2210 {
  private readonly logger = new Logger('Schema')

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getFileByType(file: string, _type: FileTypes): Promise<any> {
    try {
      return await this.getcontent(file)
    } catch (e) {
      throw new NotFoundException('Desired schema not found, schema incorrect or unimplemented')
    }
  }

  async getJsonldContext(): Promise<any> {
    try {
      const uri: string = process.env.BASE_URL
      const context = {
        self: `${uri}/api/trusted-schemas-registry/v2/schemas/`,
        item: []
      }

      const jsonFiles = await this.getAvailableSchemasFiles()
      for (const file of jsonFiles) {
        context.item.push({
          schemaId: file,
          href: `${uri}/api/trusted-schemas-registry/v2/schemas/${file}`
        })
      }

      return context
    } catch (e) {
      console.log('error', e.message)
      throw e
    }
  }

  async getAvailableSchemasFiles(): Promise<Array<string>> {
    try {
      return await fs.readdirSync(join(__dirname, `../../static/schemas`)).map(filename => filename.split('.')[0])
    } catch (e) {
      throw new NotFoundException('Desired schema not found, version is incorrect or unimplemented')
    }
  }

  private async getcontent(filename): Promise<any> {
    try {
      fs.accessSync(join(__dirname, `../../static/schemas/${filename}.json`))
      return JSON.parse(fs.readFileSync(join(__dirname, `../../static/schemas/${filename}.json`), 'utf-8'))
    } catch (error) {
      this.logger.error(error)
      throw new NotFoundException('Desired schema not found, schema name is incorrect or unimplemented')
    }
  }
}
