import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation } from '@nestjs/swagger'

export function LifecycleApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiBadRequestResponse({ description: 'Invalid request query' }),
    ApiNotFoundResponse({ description: "Lifecycle messages not found" }),
    ApiOkResponse({ description: 'All lifecycle messages used whithin the ecosystem' })
  )
}
