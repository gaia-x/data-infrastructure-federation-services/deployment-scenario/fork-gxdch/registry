import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common'
import { messages } from './constants'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { LifecycleMessages, LifecycleDocument } from './schemas/lifecycle.schema'
@Injectable()
export class LifecycleService {
  constructor(@InjectModel(LifecycleMessages.name) public lifecycleModel: Model<LifecycleDocument>) { }
  async getAvailableMessages(): Promise<any[]> {

    const query = await this.lifecycleModel.findOne({ key: "lifecycleMessages" }).exec()
    if (query === null) {
      throw new NotFoundException('Messages not found')
    }
    return query.messages

  }


  async postMessages(messages: any[]): Promise<any> {

    const query = await this.lifecycleModel
      .findOne({ key: "lifecycleMessages" })
      .exec()
      .catch(e => console.error(e))
    if (query == null) {
      const ecosystemMessages = new this.lifecycleModel({ key: "lifecycleMessages", messages: messages })
      return ecosystemMessages.save()
    } else {
      await this.lifecycleModel
        .findOneAndUpdate({ key: "lifecycleMessages" }, { key: "lifecycleMessages", messages: messages })
        .exec()
        .catch(e => console.error(e))
      return true
    }
  }
}


