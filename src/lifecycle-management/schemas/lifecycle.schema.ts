import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type LifecycleDocument = LifecycleMessages & Document

@Schema({
  timestamps: true
})
export class LifecycleMessages {

  @Prop({
    required: true,
    trim: true
  })
  public messages: any[]

  @Prop({
    required: true,
    trim: true
  })
  public key: string
}

export const LifecycleSchema = SchemaFactory.createForClass(LifecycleMessages)
