import { Module } from '@nestjs/common'
import { LifecycleService } from './lifecycle.service'
import { LifecycleController } from './lifecycle.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { LifecycleMessages, LifecycleSchema } from './schemas/lifecycle.schema'
import { ApiKeyGuard } from '../auth/auth.guard'

@Module({
  imports: [MongooseModule.forFeature([{ name: LifecycleMessages.name, schema: LifecycleSchema }])],
  controllers: [LifecycleController],
  providers: [LifecycleService, ApiKeyGuard]
})
export class LifecycleModule { }
