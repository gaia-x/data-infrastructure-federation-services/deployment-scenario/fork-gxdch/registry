export const messages = [
  {
    status: "0x00",
    value: "VALID"
  },
  {
    status: "0x01",
    value: "SUSPENDED"
  },
  {
    status: "0x10",
    value: "REVOKED"
  },
  {
    status: "0x11",
    value: "EXPIRED"
  }
]
