import { Controller, Get, Post, UsePipes, Query, Put, UseGuards, Body } from '@nestjs/common'
import { ApiBasicAuth, ApiTags, ApiSecurity, ApiBody } from '@nestjs/swagger'
import { LifecycleService } from './lifecycle.service'
import { LifecycleApiResponse } from './decorators'
import { ApiQuery } from '@nestjs/swagger'
import { ApiKeyGuard } from '../auth/auth.guard'
import { messages } from './constants'
import { messageRequestDTO } from './dto'

@ApiTags('Lifecycle-Management')
@Controller({ path: '/api/lifecycle' })
export class LifecycleController {
  constructor(private readonly lifecycleService: LifecycleService) { }

  @LifecycleApiResponse('Get the set of messages used in the lifecycle management component: the Revocation Registry')
  @Get()
  async getTermsAndConditions() {
    return this.lifecycleService.getAvailableMessages()
  }

  @LifecycleApiResponse('Update set of messages used in the lifecycle management component: the Revocation Registry')
  @ApiBody({
    isArray: true,
    examples: {
      example: {
        value: messages
      }
    },
    required: true
  })
  @ApiSecurity('X-API-KEY')
  @UseGuards(ApiKeyGuard)
  @Post()
  async postmessages(@Body() message: messageRequestDTO[]) {

    return this.lifecycleService.postMessages(message)
  }


}
