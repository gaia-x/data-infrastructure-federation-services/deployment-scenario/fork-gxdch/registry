import { Module } from '@nestjs/common'
import { ApiKeyGuard } from './auth.guard'

@Module({
  imports: [],
  controllers: [],
  providers: [ApiKeyGuard]
})
export class AuthModule {}
